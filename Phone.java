public class Phone
{
//private fields
  private int storage;
  private String model;
  private String color;
//choice method
  public void Choice()
  {
    System.out.println("I think that the color " + color + " is beautiful!");
    if(model.equals("UltraSuperProMax"))
    {
      System.out.println("Wow you are rich");
    }
  }
//setters
    public void setModel(String newModel)
  {
    this.model = newModel;
  }
  public void setColor(String newColor)
  {
    this.color = newColor;
  }
//getters
  public int getStorage()
  {
    return this.storage;
  }
  public String getModel()
  {
    return this.model;
  }  
  public String getColor()
  {
    return this.color;
  }
//constructor
  public Phone(int storage, String model, String color)
  {
    this.storage = storage;
    this.model = model;
    this.color = color;
  }
}